# 1. Groupe
Membres
- Louis MOULIN
- Anthony ARJONA
- Maxime CARLUER

Classe : 4IW3

Date : 28/06/2022

## 2. Préambule
### Description
Dealabs est un service proposé par la société Pepper France filiale de la société allemande Pepper Media. Le site propose des réductions, promotions et des bons plans commerciaux, valables en ligne et en magasin. 

- Dealabs a été créé en 2011
- En 2013, Dealabs rejoint le groupe Pepper, qui regroupe plusieurs plateformes internationales de bons plans
- En 2018, Dealabs compte 500 000 membres, 3 millions de visiteurs uniques par mois et 37 salariés

### Type d'utilisateurs
Les offres sont proposées par les internautes. Chaque membre du site peut voter pour une offre partagée par un autre internaute, renforçant ainsi sa visibilité sur le site.

L'objectif ici va être de tester le site avec une affluence normale d'utilisateur afin de voir s'il se comporte comme ce qui est attendu.
 
## 3. Architecture de l'application
### Résumé de l'architecture globale


### Stack
Coté client :
- Vue.js

Coté serveur :
- PHP

## 4. Exigence du test

| Business Transactions | User Load | Response Time | Transactions per hour |
|--------------|:-----------:|:------------:|:------------:|
| Accéder à la page de connexion | 200 000 | 1 | 1 500 000 |
| Accéder à la page d'inscription | 100 000 | 1 | 300 000 |
| Effectuer une recherche | 100 000 | 1 | 300 |
| Ajouter un produit au panier | 100 | 1 | 300 |

## 5. Environnement de test

Nous n'avons pas les informations nécessaires pour répondre à ce besoin.

## 6. Planification des tests

## 7. Etapes des tests
| Step # | Business Process Name : Product Ordering |
|--------------|:-----------:|
| 1 | Page d'accueil |
| 2 | Inscription |
| 3 | Connexion |
| 4 | Recherche d'un produit |
| 5 | Sélection d'un produit |
| 6 | Ajout d'un commentaire sur un produit |

## 8. Execution des tests
Résumé du scénario de test :

| Test Run | Test Scenario Summary |
|--------------|:-----------:|
| Smoke Test | To validate the performance test scripts and monitors |
| Cycle 1 - Run 1 | Load Test - Test de 1 heure avec charge maximale |
| Cycle 1 - Run 2 | Spike Test - Test de 1 heure avec 200% de la charge maximale |
| Cycle 2 - Run 1 | Load Test - Test de 1 heure avec charge maximale |
| Cycle 2 - Run 2 | Spike Test - Test de 1 heure avec 250% de la charge maximale |