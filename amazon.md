# 1. Groupe

Membres :

- Louis MOULIN
- Anthony ARJONA
- Maxime CARLUER

Classe : 4IW3

Date : 28/06/2022

## 2. Préambule

Nous allons étudier le site Amazon, qui est accessible depuis amazon.com.

### Description

Amazon est une entreprise de commerce en ligne américaine. Elle est l'un des géants du Web, regroupés sous l'acronyme GAFAM, aux côtés de Google, Apple, Meta et Microsoft. Créée par Jeff Bezos en juillet 1994, l'entreprise a été introduite en bourse en 1997.

Le groupe développe une activité de place de marché permettant aux particuliers et aux sociétés de distribution d'effectuer leurs transactions d'achat et de vente de biens et de services

### Type d'utilisateurs

En France, le nombre de visiteurs mensuels est de 152 millions d'utilisateurs en 2021.

Amazon propose des produits à ses visiteurs. Il faut donc que l'application web soit assez performante pour gérer un grand flux d'utilisateurs voulant simplement consulter des produits jusqu'à l'achat d'un produit.

Amazon profite d'un capital confiance de ses clients énorme car l'expérience utilisateur est la plus fluide possible pour chaque achat.

### Objectifs des tests

L'objectif ici va être de tester le site avec une affluence normale d'utilisateur afin de voir s'il se comporte comme ce qui est attendu.

Dans un second temps, nous allons aussi essayer d'identifier si le site est capable d'absorber une grosse charge d'affluence comme lors d'un évènement comme le Black Friday.

## 3. Architecture de l'application

### Résumé de l'architecture globale

Amazon possède plusieurs pages que nous allons tester :

- page d'accueil contenant
  - produit phare
  - barre de recherche de produit
  - liste des catégories
- page de recherche après saisie
  - liste des produits liés à la recherche et aux filtres
- page d'un produit
  - fiche produit
  - système de paiement
  - commentaire

### Stack

Côté client :

- Javascript

Côté serveur :

- Java
- C++
- Perl

Côté base de données :

- Oracle Database

## 4. Exigence du test

| Business Transactions           | User Load ---- | Response Time | Transactions per hour |
| ------------------------------- | :-------: | :-----------: | :-------------------: |
| Accéder à la page d'accueil  |  1 500 000  |       1       |       10 000 000       |
| Effectuer une recherche         |  1 000 000  |       2       |        7 500 000        |
| Accéder à une fiche produit  |  700 000  |       2       |       5 000 000       |
| Ajouter un produit au panier    |  100 000  |       1       |        500 000        |
| Accéder à la page de connexion  |  200 000  |       2       |       1 500 000       |
| Effectuer un paiement |  100 000  |       2       |        300 000        |

## 5. Environnement de test

Pour nos tests, nous utiliserons l'environnement suivant :

- CPU : Intel(R) Xeon(R) CPU E5-2690 v3 @ 2.60GHz
- Mémoire : 4gb
- OS : Debian 10.2.1-6

Pour la production, il faudra multiplier les performances de l'environnement de test par le nombre attendu d'utilisateur sur le site final.

L'environnement de test prévoit une charge de 100 personnes actives. La charge d'utilisateur sur le site de production est de 500 000.

Le coéficient est donc de 500 000 / 100 = 5 000.

## 6. Planification des tests

Amazon a une infrastructure en micro service, ce qui permet de s'adapter au besoin réel de l'application à tout moment.

Métriques :

- le temps moyen d'accès au site
- conversion visite / achat
- nombre de recherches

Sur un spike test, surveiller :

- temps de réponse moyen d'accès au site
- la charge du CPU
- l'utilisation de la RAM
- la bande passante utilisée

Conditions de réussite des tests :

- le temps de réponse moyen est inférieur au temps de réponse attendu (2 secondes)
- la charge CPU doit être inférieur à 90%

## 7. Etapes des tests

| Step # | Business Process Name : Product Ordering |
| ------ | :--------------------------------------: |
| 1      |              Page d'accueil              |
| 2      |                Connexion                 |
| 3      |          Recherche d'un produit          |
| 4      |          Sélection d'un produit          |
| 5      |          Ajout au panier                 |
| 6      |          Paiement d'un produit           |

## 8. Execution des tests

Résumé du scénario de test :

| Test Run        |                 Test Scenario Summary                 |
| --------------- | :---------------------------------------------------: |
| Smoke Test      | Valider les scripts de test de performance et les moniteurs |
| Cycle 1 - Run 1 |  Load Test - Test d'une heure avec une charge normale  |
| Cycle 2 - Run 1 |  Spike Test - 30 minutes de test avec 100% de la charge maximale  |
| Cycle 2 - Run 2 |  Spike Test - 30 minutes de test avec 120% de la charge maximale  |

## 9. Résultats des tests

Référencement des tests effectués :

## 10. Analyses et Optimisations proposées

Analyse des résultats :
