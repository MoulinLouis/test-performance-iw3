## Environnement de test en production

- CPU : Intel(R) Xeon(R) CPU E5-2690 v3 @ 2.60GHz
- Mémoire : 4gb
- OS : Debian 10.2.1-6

## Environnement de test client JMeter

- CPU : Intel Core i5-9300H CPU @ 2.40GHz
- Mémoire : 8gb
- OS : Windows 10

## Scénario de test

### Premier scénario - **performance** - spike testing

S'assurer que les pages disponible en tant qu'utilisateur non-authentifié aient un temps de réponse correct lorsqu'une certaine charge d'utilisateurs se connectent au site.

| Business Transactions | User Load | Response Time | Transactions per hour |
|--------------|:-----------:|:------------:|:------------:|
| Accéder à la page d'accueil | 100 | 3 | 3000 |
| Accéder à la page des combattants | 100 | 3 | 2000 |
| Accéder à la page des combats | 100 | 3 | 2000 |
| Accéder à la page de connexion | 100 | 1 | 2500 |
| Accéder à la page d'inscription | 100 | 1 | 2500 |

### Second scénario - **parcours utilisateur** - load testing

S'assurer que le parcours utilisateur d'un parieur fonctionne comme attendu avec des temps de réponse correct.

<!-- note: la connexion ne fonctionne pas car notre formulaire nécessite le csrf_token généré à la création du formulaire. -->

| Business Transactions | User Load | Response Time | Transactions per hour |
|--------------|:-----------:|:------------:|:------------:|
| Accéder à la page de connexion | 20 | 2 | 2000 |
| Connexion avec un compte utilisateur | 20 | 4 | 1000 |
| Accéder à ma page de profil | 20 | 2 | 1000 |
| Accéder à mes abonnements | 20 | 2 | 1000 |
| Accéder à mes paris | 20 | 2 | 1000 |
| Accéder à mes transactions | 20 | 2 | 1000 |

Question :
- pourquoi vos tests ?

Choix de faire un spike test sur les pages les plus parcouru du site (accueil, fiche de combat, fiche de combattant) car ces pages seront fortement utilisé lors du début d'un combat

- qu'est ce que le volume testing
