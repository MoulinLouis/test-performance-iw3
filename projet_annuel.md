# 1. Groupe
Membres
- Louis MOULIN
- Anthony ARJONA
- Maxime CARLUER

Classe : 4IW3

Date : 28/06/2022

## 2. Préambule
Nous allons étudier le site de notre projet annuel, Fightbet, qui est accessible depuis fightbet.fr.

### Description globale du projet
Ce projet permet de visualier et de pouvoir parier sur les combats de nos professeurs.

### Objectif de l'application
L'objectif de ce test de performance est de pouvoir déterminer les limites de notre projet annuel pour permettre l'accès au site et aux actions du site :
- en quantité d'utilisateurs connectés

### Type d'utilisateurs prévus
Fightbet prévoit d'accueillir de jeunes utilisateurs (18-25 ans en général). On estime qu'on peut avoir au maximum le nombre d'élèves dans l'école connectés en même temps.

## 3. Architecture de l'application
### Résumé de l'architecture globale
Cette application possèdes plusieurs pages :
- Accueil
- Compte
    - Profil
    - Abonnements
- Combattants
    - Liste des combattants
    - Une page dédié à un combattant selectionné
- Combats
    - Liste des combats
    - Une page dédié à un combat selectionné
- Arbitrage

### Technologies/Langages utilisés
Notre application est une application Symfony (PHP) utilisant Twig et une base de données PostgreSQL

### Composants/Services
Nous utilisons plusieurs composants et services supplémentaires :
- RabbitMQ
- Mercure
- Redis

### Production
L'infrastructure de production est un serveur Web en front nginx qui répartit les requêtes vers un serveur back PHP qui lui même communique avec une base de données PostgreSQL.

## 4. Exigence du test

| Business Transactions | User Load | Response Time | Transactions per hour |
|--------------|:-----------:|:------------:|:------------:|
| Accéder à la page de connexion | 200 000 | 1 | 1 500 000 |
| Accéder à la page d'inscription | 100 000 | 1 | 300 000 |
| Effectuer une recherche | 100 000 | 1 | 300 |
| Ajouter un produit au panier | 100 | 1 | 300 |

## 5. Environnement de test

- CPU : Intel(R) Xeon(R) CPU E5-2690 v3 @ 2.60GHz
- Mémoire : 4gb
- OS : Debian 10.2.1-6

## 6. Planification des tests
TODO

